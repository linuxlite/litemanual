Lite Manual
================

The Linux Lite Help and Support Manual with built-in search engine. Available offline on Linux Lite, or viewable
online [here](https://www.linuxliteos.com/manual).

![](https://imgur.com/6IW04Yi.png)

## Offline search feature:

![](https://imgur.com/or1ukYJ.png)

## License ![License](https://img.shields.io/badge/license-GPLv2-green.svg)

This project is under the GPLv2 license. Unless otherwise stated in individual files.

## Authors
- [Adam Grubbs](https://github.com/argrubbs/)
- [Bill Hahnen](https://github.com/gold-finger/)
- [Jerry Bezencon](https://github.com/linuxlite/)
- [Johnathan "ShaggyTwoDope" Jenkins](https://github.com/shaggytwodope/)
- [Misko-2083](https://github.com/Misko-2083/)
- [Ralphy](https://github.com/ralphys/)
